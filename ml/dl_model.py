import time
from pathlib import Path
from typing import Sequence, Union

import cv2
import fastai
import fastai.vision
import numpy as np
from PIL import Image


# import torch
# import torchvision
# import torchvision.transforms

def pil2cv(image):
    ''' PIL型 -> OpenCV型 '''
    new_image = np.array(image, dtype=np.uint8)
    if new_image.ndim == 2:  # モノクロ
        pass
    elif new_image.shape[2] == 3:  # カラー
        new_image = new_image[:, :, ::-1]
    elif new_image.shape[2] == 4:  # 透過
        new_image = new_image[:, :, [2, 1, 0, 3]]
    return new_image


def cv2pil(image):
    ''' OpenCV型 -> PIL型 '''
    new_image = image.copy()
    if new_image.ndim == 2:  # モノクロ
        pass
    elif new_image.shape[2] == 3:  # カラー
        new_image = new_image[:, :, ::-1]
    elif new_image.shape[2] == 4:  # 透過
        new_image = new_image[:, :, [2, 1, 0, 3]]
    new_image = Image.fromarray(new_image)
    return new_image


class Resnet34Classifier(object):
    def __init__(self, path: str, file_name: str, size: int):
        self.__path = Path(path)
        self.__file_name = file_name  # 読み込むファイル名「*.pkl
        self.__size = size  # 入力画像 size x size
        self.__learn = fastai.vision.load_learner(self.__path, file=self.__file_name)  # 古いバージョンには無い

    def __call__(self, img: np.ndarray, *args, **kwargs):
        """
        :param img:  opencvで読み込んだ画像データを想定
        :param args:
        :param kwargs:
        :return:
        """

        # start = time.time()

        pred_class = self.predict(img)

        # process_time = time.time() - start
        # print(process_time)
        # print(pred_class)

        return pred_class

    def predict(self, img: np.ndarray):
        """
        :param img: opencvで読み込んだ画像データを想定
        :return: 各クラスの確率が格納された配列
        """
        pil_img = cv2pil(img)  # pillow画像にする
        x = fastai.vision.pil2tensor(pil_img, np.float32)  # tensorにする

        start = time.time()
        pred_class, pred_idx, outputs = self.__learn.predict(fastai.vision.Image(x))
        process_time = time.time() - start

        print(process_time)
        print(pred_class)

        return pred_class


class Resnet34ClassifierNG(object):
    def __init__(self, path: str, file_name: str, classes: Sequence[Union[int, str]], size: int):
        self.__path = Path(path)
        self.__file_name = file_name  # 読み込むファイル名「*.pth」の「*」
        self.__classes = classes
        self.__size = size  # 入力画像 size x size
        self.__norm_status = fastai.vision.imagenet_stats
        self.__tfms = fastai.vision.get_transforms()

        self.__data = fastai.vision.ImageDataBunch.single_from_classes(
            self.__path,
            self.__classes,
            ds_tfms=self.__tfms,
            size=self.__size
        ).normalize(self.__norm_status)

        self.__arch = fastai.vision.models.resnet34  # モデルの構造(時間が掛かりすぎるのでresnet18がいいかも)
        # self.__learn = fastai.vision.cnn_learner(self.__data, self.__arch)  # 1.0.61
        self.__learn = fastai.vision.create_cnn(self.__data, self.__arch)  # 古いやつよう

        # モデルの読み込み
        # モデルの保存や読み込みは、設定したpath直下に配置した
        # 「models」というディレクトリ内のファイルに対して実施される
        self.__learn.load(self.__file_name)

    def __call__(self, img: np.ndarray, *args, **kwargs):
        """
        :param img:  opencvで読み込んだ画像データを想定
        :param args:
        :param kwargs:
        :return:
        """

        # start = time.time()

        preds_num = self.predict(img)

        # process_time = time.time() - start
        # print(process_time)
        # print(preds_num)
        return self.pred_class(preds_num)

    def predict(self, img: np.ndarray):
        """
        :param img: opencvで読み込んだ画像データを想定
        :return: 各クラスの確率が格納された配列
        """
        pil_img = cv2pil(img)  # pillow画像にする
        x = fastai.vision.pil2tensor(pil_img, np.float32)  # tensorにする

        start = time.time()
        preds_num = self.__learn.predict(fastai.vision.Image(x))[2]
        process_time = time.time() - start
        print(process_time)
        print(preds_num)

        return np.asarray(preds_num)

    def pred_class(self, preds_num: np.ndarray):
        """
        :param preds_num:各クラスの確率が格納された配列
        :return:
        """
        return self.__classes[np.argmax(preds_num)]


def main():
    # path = 'ml'
    #
    # # model_file = 'resnet34_128x128'
    # model_file = 'resnet34_224x224'
    # classes = ['a', 'b', 'c', 'd']
    # # size = 128
    # size = 224
    #
    # # 学習済みのモデルを生成
    # net = Resnet34Classifier(
    #     path=path,
    #     file_name=model_file,
    #     classes=classes,
    #     size=size
    # )

    path = 'ml'
    model_model = 'trained_model.pkl'
    size = 224

    net = Resnet34Classifier(
        path=path,
        file_name=model_model,
        size=size
    )

    # opencvで画像を読み込む
    test_img = 'ml/tes_img/processing_00.jpg'
    img = cv2.imread(test_img)
    img = cv2.resize(img, (size, size))

    # モデルでクラスを予測
    pred_class = net(img)


if __name__ == "__main__":
    main()
